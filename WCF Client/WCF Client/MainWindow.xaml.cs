﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WCF_Client.DataTransfer;
using System.Globalization;

namespace WCF_Client
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        DataTransferClient client;

        public MainWindow()
        {
            InitializeComponent();

            App.LanguageChanged += LanguageChanged;
            var currLang = App.Language;

            foreach (var lang in App.Languages)
            {
                var menuLang = new MenuItem();
                menuLang.Header = lang.DisplayName;
                menuLang.Tag = lang;
                menuLang.IsChecked = lang.Equals(currLang);
                menuLang.Click += ChangeLanguageClick;
                menuLanguage.Items.Add(menuLang);
            }
        }

        private void LanguageChanged(object sender, EventArgs e)
        {
            var currLang = App.Language;
            foreach (MenuItem item in menuLanguage.Items)
            {
                var cultureInfo = item.Tag as CultureInfo;
                item.IsChecked = cultureInfo != null && cultureInfo.Equals(currLang);
            }
        }

        private void ChangeLanguageClick(object sender, RoutedEventArgs e)
        {
            var menuItem = sender as MenuItem;
            if (menuItem != null)
            {
                var lang = menuItem.Tag as CultureInfo;
                if (lang != null)
                {
                    App.Language = lang;
                }
            }
        }

        private async void bGetDate_Click(object sender, RoutedEventArgs e)
        {
            bGetDate.IsEnabled = false;
            try
            {
                if (client.State == System.ServiceModel.CommunicationState.Faulted)
                {
                    client = GetClient();
                }

                var data = new ObservableCollection<DataTransfer.Point>(await client.GetPointsAsync(180));
                SparrowChart.DataContext = null;
                SparrowChart.DataContext = data;

                lInfo.SetResourceReference(Label.ContentProperty, "Connection_Connected");
            }
            catch 
            {
                lInfo.SetResourceReference(Label.ContentProperty, "Connection_Failure");
            }
            bGetDate.IsEnabled = true;
        }

        private async void bGetMoreDate_Click(object sender, RoutedEventArgs e)
        {
            bGetMoreDate.IsEnabled = false;
            try
            {
                if (client.State == System.ServiceModel.CommunicationState.Faulted)
                {
                    client = GetClient();
                }

                ///получаем все данные чтобы загрузить канал, но берём только 200 чтобы график не тормозил
                var points = await client.GetPointsAsync(10000000);
                var data = new ObservableCollection<DataTransfer.Point>(points.Take(200));

                SparrowChart.DataContext = null;
                SparrowChart.DataContext = data;

                lInfo.SetResourceReference(Label.ContentProperty, "Connection_Connected");
            }
            catch (Exception ex)
            {
                string s = ex.ToString();
                lInfo.SetResourceReference(Label.ContentProperty, "Connection_Failure");
            }
            bGetMoreDate.IsEnabled = true;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            client = GetClient();
        }

        DataTransferClient GetClient()
        {
            return new DataTransferClient("NetTcpBinding_IDataTransfer");
        }

    }
}
