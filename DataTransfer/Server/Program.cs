﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var host = new ServiceHost(typeof(DataTransfer.DataTransfer)))
            {
                try
                {
                    host.Open();
                    Console.WriteLine("Сервер запущен...");
                }
                catch (AddressAccessDeniedException)
                {
                    Console.WriteLine("Невозможно запустить сервер: Доступ запрещён, попробуйте запустить приложение с правами администратора");
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Невозможно запустить сервер:");
                    Console.WriteLine(ex.ToString());
                }
                
                Console.ReadLine();
            }
        }
    }
}
