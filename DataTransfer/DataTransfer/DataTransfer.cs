﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Drawing;

namespace DataTransfer
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerSession)]
    public class DataTransfer : IDataTransfer
    {
        int position = 0;

        public List<Point> GetPoints(int size)
        {
            Console.WriteLine("Запрос " + size + " точек");
            var points = new List<Point>();
            int maxSize = 1000000;
            if (size <= 0 && size > maxSize)
            {
                Console.WriteLine("Параметр должен быть больше 0 и меньше чем " + maxSize);
                return points;
            }

            for (int i = 0; i < size; i++)
            {
                int y = (int)(100 * Math.Sin(Math.PI * 4 * (i + position) / 90) + 100);
                var point = new Point(i, y);
                points.Add(point);
            }

            position += 10;
            Console.WriteLine("Выдано " + points.Count + " точек\n");
            return points;
        }
    }
}
