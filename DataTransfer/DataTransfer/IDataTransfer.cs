﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace DataTransfer
{
    [ServiceContract]
    public interface IDataTransfer
    {
        [OperationContract]
        List<Point> GetPoints(int size);

    }
}
